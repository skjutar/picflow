var Subscription = require('../models/subscription.js');

exports.registerFollower = function(type, value, callback) {
	Subscription.findOne({type: type, value: value}, function(err, subscription) {
		console.log(subscription);
		if(err || !subscription) { 
			console.log("creating a subscription for " + value);
			subscription = new Subscription({type: type, value: value, subscribers: 1, id: 0});
			subscription.save(function() {
				callback(true);
			});
		} else {
						console.log("updating a subscription for " + value);
			subscription.subscribers++;
			subscription.save(callback());
		}
	});
}

exports.setID = function(type, value, id) {
	Subscription.findOneAndUpdate({type: type, value: value}, {$set: {id: id} }, function() {

	});
} 

exports.removeFollower = function(type, value, callback) {
	Subscription.findOne({type: type, value: value}, function(err, subscription) {
		if(!subscription) {
			callback();
		} else {
			console.log(subscription);
			subscription.subscribers--;
			if(subscription.subscribers === 0) {
				callback(subscription.id);
				subscription.remove();
			} else {
				subscription.save();
				callback();
			}
		}
	})
	
}