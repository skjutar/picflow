'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
.controller('MyCtrl1', ['$scope','mySocket', '$http', 'instaMedia', function($scope, mySocket, $http, instaMedia) {
	$scope.focus = false;
	$scope.data = [];
	$scope.itemsReadyForData = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17];
	$scope.modes = 
	[
	{name: "Hashtag", symbol:"#", holder: "Search for a hashtag"},
	{name: "Place", symbol: "Place", holder: "Search for a place"}
	]
	$scope.toggleMode = function(mode) {
		$scope.input = "";
		$scope.currentMode = mode;
	}
	$scope.currentMode = $scope.modes[0];

	$scope.isFocused = function() {
		return $scope.focus;
	}
	$scope.updateFlow = function() {
		$scope.data = [];
		initMedias(function(){
			mySocket.emit("hastag_flow", {hashtag: $scope.input});
		});
	}
	$scope.readyForNewData = function(index) {
		//console.log("adding " + index + " to " + $scope.itemsReadyForData);
		if($scope.itemsReadyForData.indexOf(index) === -1) {
			$scope.itemsReadyForData.push(index);
		}
	}
	mySocket.on("init", function(msg) {
		instaMedia.setClient(msg.id);
		initMedias();
	});

	var initMedias = function(callback) {
		if($scope.data === undefined || $scope.data.length === 0) {
			instaMedia.grabTag($scope.input, 18).then(function(medias) {
				$scope.data = medias;
				if(callback) {
					callback();
				}
			})
		}
	}

	var distributeMedia = function(medias) {
		for (var i = 0; i < medias.length; i++) {
			var readyItems = $scope.itemsReadyForData.length;
			if(readyItems > 0) {
				var selected = Math.floor(Math.random() * readyItems);
				//console.log("choosed " + $scope.itemsReadyForData[selected] + " from all the " + readyItems + " in " + $scope.itemsReadyForData);
				$scope.data[$scope.itemsReadyForData[selected]] = medias[i];
				$scope.itemsReadyForData.splice(selected, 1);
			}
		};
	}


	mySocket.on("instaEvent", function(msg) {
		var instaUrl;
		switch(msg.mode) {
			case "tags":
			instaMedia.grabTag(msg.object_id, 1).then(function(medias) {
				distributeMedia(medias);
			})
			break;
		}
	});

}])
.controller('MyCtrl2', ['$scope', function($scope) {

}]);
