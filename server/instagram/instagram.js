var instagram = require('instagram-node-lib');
var fs = require('fs');
var request = require('request');
instagram.set('client_id', 'YOUR-CLIENT-ID');
instagram.set('client_secret', 'YOUR-CLIENT-SECRET');
var clientID;
var followerDB = require("./followerDB.js");

exports.setUp = function(callback) {
	var file = __dirname + '/credentials.json';

	fs.readFile(file, 'utf8', function (err, data) {
		if (err) {
			console.log('Error: ' + err);
			return;
		}

		data = JSON.parse(data);
		instagram.set('client_id', data.clientID);
		clientID = data.clientID;
		instagram.set('client_secret', data.clientSecret);
		instagram.set('callback_url', 'http://skjutar.se:3000/instaEvent');

		//Delete old subscription manually
		request({uri : "https://api.instagram.com/v1/subscriptions?client_secret=" + data.clientSecret + "&object=tag&client_id="+clientID,
			method: "DELETE"}, function(err, response, body) {
				console.log("instagram connected with the following credentials: \n " + data.clientID + " \n " + data.clientSecret);
				if(callback) {
					callback();
				}
			});
	});
}

exports.createHashTagSubscription = function(hashtag) {
	followerDB.registerFollower("HASHTAG", hashtag, function(needToSubscribe) {
		if(needToSubscribe) {
			var result = instagram.tags.subscribe({ object_id: hashtag, verify_token: hashtag, complete: function(data) {
				console.log(data);
				followerDB.setID("HASHTAG", hashtag, data.id);
			} });
		}
	});
}

exports.removeHashTagSubscription = function(hashtag) {
	console.log("removing sub with hastag " + hashtag);
	followerDB.removeFollower("HASHTAG", hashtag, function(id) {
		console.log("id: " + id);
		if(id) {
			instagram.tags.unsubscribe({id: id});
		}
	})
}

exports.getClientID = function() {
	return clientID;
}