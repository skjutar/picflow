var socket = require("socket.io");
var instagram = require("../Instagram/instagram.js")
exports.init = function(server) {
	io = require('socket.io')(server);
	io.on('connection', function(socket) {
		console.log("a user connected");
		socket.on('disconnect', function(){
			instagram.removeHashTagSubscription(socket.instaValue);
		});

		socket.on("hastag_flow", function(data) {
			console.log(data);
			socket.emit('init', {id: instagram.getClientID()});
			if(socket.instaMode && socket.instaValue) {
				instagram.removeHashTagSubscription(socket.instaValue);
			}
			console.log("incoming hashtag call...");
			socket.join(data.hashtag);
			socket.instaMode = "HASHTAG";
			socket.instaValue = data.hashtag;
			instagram.createHashTagSubscription(data.hashtag);
		});

		socket.on("geo_flow", function(id, msg) {
			//get geoID and let socket join that
		  	//TODO: handle disconnect
		  })

	});
	console.log("socket.io has been established")
	instagram.setUp();
}





exports.instaEvent = function(req, res) {
	console.log("instaEvent!!")
	res.send();
	req.body.forEach(function(event) {
		switch(event.object) {
			case "tag": 
			io.to(event.object_id).emit("instaEvent", {mode: "tags", object_id: event.object_id, client_id: instagram.getClientID()});
			break;

			case "geography":
			break;
		}
	})
}

exports.instaVerify = function(req, res) {
	console.log("challenge completed")
	res.send(req.query['hub.challenge']);
}