"use strict";
var express = require('express');
var socket = require('./routes/socket.js');
var Subscription = require('./models/subscription.js');
var app = module.exports = express();

var mongoose = require('mongoose');



mongoose.connect("mongodb://localhost/picflow")

var db = mongoose.connection;
db.on('error', function() {console.log('#########connection error to mongodb#########')});



//Config
app.configure(function(){
   app.set('port', process.env.PORT || 3000);
   app.set('views', './app');
   app.engine('.html', require('ejs').renderFile);
   app.set('view engine', 'html');
   app.use(express.json());
   app.use(express.urlencoded());
   app.use(express.methodOverride());
   app.use(app.router);
   app.use(express.favicon());
   app.use(express.logger('dev'));
   app.use(express.static('./app'));
});


// Angular Routes
var routes = require('./routes/index.js');
app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

//RESTful API
/**var api = require('./routes/api.js');
app.post('/thread', api.post);
app.get('/thread/:title.:format?', api.show);
app.get('/thread', api.list); */
app.post('/instaEvent', socket.instaEvent);
app.get('/instaEvent', socket.instaVerify);

//Start server
var server = app.listen(app.get('port'), function(){
   Subscription.remove({}, function(err) {});
  console.log("Server listening on port %d", server.address().port);
  socket.init(server);
});





