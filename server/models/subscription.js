// The Thread model
 
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
 
var subscriptionSchema = new Schema({
    id:  String,
    type: String,
    value: String,
    subscribers: Number
});
 
module.exports = mongoose.model('Subscription', subscriptionSchema);