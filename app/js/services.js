'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
var service = angular.module('myApp.services', ['btford.socket-io']);


service.factory('mySocket', function (socketFactory, instaMedia) {
	var myIoSocket = io.connect("http://picflow.skjutar.se:3000");
	var mySocket = socketFactory({
		ioSocket: myIoSocket
	});

	return mySocket;
});

service.factory('instaMedia', function($http, $q) {
	var deferred;
	var client_id;
	return {
		grabTag: function(hashtag, amount) {
			deferred = $q.defer();
			var instaUrl = 'https://api.instagram.com/v1/tags/' + hashtag + '/media/recent?client_id= ' + client_id + '&count=' + amount + '&callback=JSON_CALLBACK';
			$http.jsonp(instaUrl).
			success(function(data, status, headers, config) {
				deferred.resolve(data.data);
			});
			return deferred.promise;

		},
		setClient: function(id) {
			client_id = id;
		}
	}
});