# Picflow

A realtime instagramviewer using AngularJS, mongoDB, node.js and socket.io.

## Running

start MongoDB:
```
  mongod
  mongo
  use picflow
```
start webbapp:
```
  npm install
  npm start
```
