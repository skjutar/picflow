'use strict';

/* Directives */

var app = angular.module('myApp.directives', []);


app.directive('appVersion', ['version', function(version) {
	return function(scope, elm, attrs) {
		elm.text(version);
	};
}]);



app.directive('ngInstaphoto', function() {
	return {
		restrict: 'E',
		scope: {
			ngContent: '=',
			ready: '&'
		},
		templateUrl: './templates/instaContent.html',
		controller: function($scope, $timeout) {
			$scope.video = false;
			$scope.firstTime = true;
			$scope.visible = false;
			$scope.show = function() {
				return $scope.visible;
			}
			$scope.hide = function() {
				return !$scope.visible;
			}

			$scope.$watch('ngContent', function() {
				if($scope.firstTime === true) {
					$scope.firstTime = false;
				} else {
					$scope.visible = false;
				}
				$timeout(function() {
					switch($scope.ngContent.type) {
						case "image":
						$scope.video = false;
						$scope.content = {
							imageURL: $scope.ngContent.images.standard_resolution.url,
							user: $scope.ngContent.user.username,
							text: $scope.ngContent.likes.count + " likes" 
						}
						break;

						case "video":
						$scope.video = true;
						console.log($scope.ngContent.videos.standard_resolution.url);
						$scope.content = {
							videoURL: $scope.ngContent.videos.standard_resolution.url,
							user: $scope.ngContent.user.username,
							text: $scope.ngContent.likes.count + " likes" 
						}
						break; 
					}
				}, 500);
			})
		}
	}
});

app.directive('instaVideo', function(){
	return {
		restrict: 'A',
		transclude: true,
		link:  function(scope, element, attr){
			var video = element[0];
			video.onended = function() {
				if(scope.video === true) {
				console.log("video ended");
				scope.ready();
			}
			};
			video.oncanplaythrough = function() {
				scope.visible = true;
				video.play();
			};
			video.onclick = function() {
				video.muted = false;
				video.play()
			};
			video.onmouseout = function() {
				video.muted = true;
			}
		}
	};
});

app.directive('instaPhoto', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		transclude: true,
		link: function(scope, element, attrs) {
			element.bind('load', function() {
				if(scope.video === false) {
					scope.visible = true;
					scope.$apply();
					$timeout(function() {
						scope.ready();
					}, 5000);
				}
			});
		}
	};
}]);



app.directive('a', function() {
	return {
		restrict: 'E',
		link: function(scope, elem, attrs) {
			if(attrs.ngClick){
				elem.on('click', function(e){
					e.preventDefault();
				});
			}
		}
	};
});

app.directive('ngEnter', function() {
	return function(scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if(event.which === 13) {
				this.blur();
				scope.$apply(function(){
					scope.$eval(attrs.ngEnter, {'event': event});
				});

				event.preventDefault();
			}
		});
	};
});

